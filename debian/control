Source: lomiri-indicator-transfer
Section: misc
Priority: optional
Maintainer: UBports Developers <devsq@ubports.com>
Build-Depends: debhelper-compat (=12),
               cmake,
               dbus,
               libglib2.0-dev (>= 2.35.4),
               libproperties-cpp-dev,
               libclick-0.4-dev (>= 0.4.30),
               libjson-glib-dev,
               liblomiri-app-launch-dev,
# for coverage reports
               lcov,
# for tests
               cmake-extras,
               cppcheck,
               dbus-test-runner,
               libgtest-dev,
               libdbustest1-dev,
               google-mock (>= 1.8.0),
               valgrind [amd64 armhf i386 powerpc],
# for systemd unit directory
               systemd-dev,
Standards-Version: 3.9.5
Homepage: https://gitlab.com/ubports/development/core/lomiri-indicator-transfer
Vcs-Git: https://gitlab.com/ubports/development/core/lomiri-indicator-transfer.git
Vcs-Browser: https://gitlab.com/ubports/development/core/lomiri-indicator-transfer
X-Ubuntu-Use-Langpack: yes

Package: lomiri-indicator-transfer
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         lomiri-indicator-transfer-common (>= ${source:Version}),
         liblomiri-indicator-transfer0 (= ${binary:Version}),
Recommends: indicator-applet | indicator-renderer,
            lomiri-content-hub,
            lomiri-download-manager,
            lomiri-indicator-transfer-download-manager (>= ${binary:Version}),
Replaces: indicator-transfer (<< 1.1.0)
Breaks: indicator-transfer (<< 1.1.0)
Provides: indicator-transfer (= ${binary:Version})
Description: Indicator showing transfers
 Indicator which shows file/data transfers in the indicator bar.

Package: indicator-transfer
Depends: lomiri-indicator-transfer, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: lomiri-indicator-transfer-common
Architecture: all
Depends: ${misc:Depends},
Replaces: indicator-transfer-common (<< 1.1.0)
Breaks: indicator-transfer-common (<< 1.1.0)
Provides: indicator-transfer-common (= ${binary:Version})
Description: Indicator showing transfers - common files
 Indicator which shows file/data transfers in the indicator bar.
 .
 This package provides architecture independent files, such as translation
 files.

Package: indicator-transfer-common
Depends: lomiri-indicator-transfer-common, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: lomiri-indicator-transfer-download-manager
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         lomiri-indicator-transfer (= ${binary:Version}),
Replaces: indicator-transfer-download-manager (<< 1.1.0)
Breaks: indicator-transfer-download-manager (<< 1.1.0)
Provides: indicator-transfer-download-manager (= ${binary:Version})
Description: Download manager plugin for transfer indicator
 Show file/data transfers in the indicator bar.

Package: indicator-transfer-download-manager
Depends: lomiri-indicator-transfer-download-manager, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: liblomiri-indicator-transfer0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
Description: Shared library used by transfer-indicator and plugins
 Indicator which shows file/data transfers in the indicator bar.

Package: liblomiri-indicator-transfer-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         liblomiri-indicator-transfer0 (= ${binary:Version}),
Description: Development files for indicator-transfer
 Show file/data transfers in the indicator bar.
