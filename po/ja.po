# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-03 15:52+0000\n"
"PO-Revision-Date: 2024-08-06 02:17+0000\n"
"Last-Translator: umesaburo sagawa <atowa-notonare-yamatonare427@pm.me>\n"
"Language-Team: Japanese <https://hosted.weblate.org/projects/lomiri/"
"lomiri-indicator-transfer/ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.7-dev\n"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:444
msgid "Files"
msgstr "ファイル"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:549
msgid "Successful Transfers"
msgstr "転送に成功しました"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:550
msgid "Clear all"
msgstr "すべてクリア"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:556
msgid "Resume all"
msgstr "すべて再開"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:562
msgid "Pause all"
msgstr "すべて一時停止"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:627
#, c-format
msgid "Unknown Download (%s)"
msgstr "不明なダウンロード (%s)"
